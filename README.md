The goal of this repository is to showcase gitlab continuous integration together with docker.

The basic idea is that we have a private r package in the repository. Every time we commit changes to this repository gitlab will start a new CI/CD pipeline which goes through 2 stages:
Build
Test

In building stage it will build a new docker image based on the rocker/hadleyverse image. This image already contains r-base and has devtools pre-installed. After retrieving this image it will install our private package and push it to my docker hub. Note that docker hub is public, for clients we want to have a private image registry to which we push the images (like we have for RB).

The testing stage will run the image that the building stage pushed and execute a unit test. 

Note that I am using a throwaway account on docker hub:
account: anchormendocker
pass: @nch0rm3n!